export default function checkRequiredFields(requiredFields, fields) {
  const allFields = Object.entries(fields);

  for (const field of requiredFields) {
    const found = allFields.find((f) => f[0] === field);
    if (!found || !found[1]) {
      return false;
    }
  }

  return true;
}
