import React from 'react';
import "./assets/scss/main.scss";

import HomePage from "./ui/pages/Home";

function App() {
  return (
    <div className="App">
      <HomePage />
    </div>
  );
}

export default App;
