import React, { Fragment } from 'react';
import footer_logo from "../../../assets/images/logo-alt.svg";
import beonshop_logo from "../../../assets/images/beonshop-logo.png";

export default function Footer(){
    return(
        <Fragment>
            <div className="Footer">
                <img src={footer_logo} alt="Bellify" className="Footer-logo"/>
            </div>
            <div className="Footer-lower">
                <div className="Footer-wrapper">
                    <span className="Footer-copyright">
                        &copy;&nbsp;2020. Bellify. All rights reserved.
                    </span>
                    <div className="Footer-beonshop">
                        <span>Work done by</span>
                        <img src={beonshop_logo} alt="Bellify"/>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}