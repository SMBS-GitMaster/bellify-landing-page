import React from "react";
import MaterialInput from "../MaterialInput";

export default function Step3(props) {
  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Qué productos o marcas utilizas en tu salón de manera habitual?
        </span>
      </div>
      <div className="col-12">
        <MaterialInput
          id="usual_product"
          onChange={props.handleChange}
          name="usual_product"
          label="Escribe aquí tu respuesta"
        />
      </div>
    </div>
  );
}
