import React, { useState } from "react";
import MaterialInput from "../MaterialInput";

export default function Step7(props) {
  const [disableInput, setDisableInput] = useState(true);

  const handleChange = ({ target: { name, checked } }) => {
    props.handleChange(name, checked);
  };

  const handleTogglerCheck = ({ target: { checked } }) => {
    setDisableInput(!checked);
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Tienes algún inconveniente para desplazarte a prestar servicios a
          domicilio del cliente?
        </span>
      </div>
      <div className="BenefitsForm-checkRow">
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            name="have_inconvenient"
            onChange={handleTogglerCheck}
            value={true}
            id="home_service"
          />
          <label className="pl-2" htmlFor="home_service">
            Si
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            name="have_inconvenient"
            onChange={handleChange}
            value={false}
            id="home_service2"
          />
          <label className="pl-2" htmlFor="home_service2">
            No
          </label>
        </div>
      </div>
      <div className="col-12">
        <MaterialInput
          name="have_inconvenient_reason"
          onChange={props.handleChange}
          id="allergic"
          label="En caso afirmativo, indica aquí el motivo por favor"
          disabled={disableInput}
        />
      </div>
    </div>
  );
}
