import React, { Fragment, useState } from "react";
import MaterialInput from "../MaterialInput";

export default function Step4(props) {
  const [disableInput, setDisableInput] = useState(true);

  const handleChange = ({ target: { name, checked } }) => {
    props.handleChange(name, checked);
  };

  const handleTogglerCheck = ({ target: { name, checked } }) => {
    props.handleChange(name, checked);
    setDisableInput(!checked);
  };

  const inputLabel = () => {
    return (
      <Fragment>
        <span className="d-none d-sm-inline">
          En caso de seleccionar Trabajador Salón,{" "}
        </span>
        <span>Indica aquí el nombre completo del salón por favor</span>
      </Fragment>
    );
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Eres un profesional autónomo o estás adscrito a algún salón de
          belleza?
        </span>
      </div>
      <div className="BenefitsForm-checkRow">
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="is_freelance"
            id="allergic_dog"
          />
          <label className="pl-2" htmlFor="allergic_dog">
            Autónomo
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleTogglerCheck}
            name="is_employee"
            id="allergic_cat"
          />
          <label className="pl-2" htmlFor="allergic_cat">
            Trabajador Salón de belleza
          </label>
        </div>
      </div>
      <div className="col-12">
        <MaterialInput
          name="salon_name"
          onChange={props.handleChange}
          id="allergic"
          label={inputLabel}
          disabled={disableInput}
        />
      </div>
    </div>
  );
}
