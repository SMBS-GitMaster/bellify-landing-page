import React from "react";

export default function Step8(props) {
  const handleChange = ({ target: { name, value } }) => {
    props.handleChange(name, value);
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Qué es lo que te motiva a nivel profesional?
        </span>
      </div>
      <div className="col-12">
        <textarea
          className="BenefitsForm-textArea p-4"
          onChange={handleChange}
          name="professional_motivation"
          rows="6"
          placeholder="Escribe aqui tu respuesta"
        />
      </div>
    </div>
  );
}
