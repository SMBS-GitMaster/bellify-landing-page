import React from "react";

export default function Step8(props) {
  const handleChange = ({ target: { name, value } }) => {
    props.handleChange(name, value);
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Cuando podrías incorporarte a Bellify?
        </span>
      </div>
      <div className="col-12">
        <input
          type="date"
          className="d-block p-4 w-100"
          onChange={handleChange}
          name="start_date"
          id="start_date"
        />
      </div>
    </div>
  );
}
