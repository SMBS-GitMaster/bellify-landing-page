import React from "react";

export default function Step4(props) {
  const handleChange = ({ target: { name, value, checked } }) => {
    if (checked) {
      props.handleChange(name, value);
    } else {
      props.handleChange(name, null);
    }
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Cómo te desplazas habitualmente?
        </span>
      </div>
      <div className="BenefitsForm-checkRow">
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="transport"
            value="Transporte público"
            id="transport1"
          />
          <label className="pl-2" htmlFor="transport1">
            Transporte público
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="transport"
            value="Coche"
            id="transport2"
          />
          <label className="pl-2" htmlFor="transport2">
            Coche
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="transport"
            value="Motocicleta"
            id="transport3"
          />
          <label className="pl-2" htmlFor="transport3">
            Motocicleta
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="transport"
            value="Andando"
            id="transport4"
          />
          <label className="pl-2" htmlFor="transport4">
            Andando
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="transport"
            value="Bici/patinete"
            id="transport5"
          />
          <label className="pl-2" htmlFor="transport5">
            Bici/patinete
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="transport"
            value="Otros"
            id="transport6"
          />
          <label className="pl-2" htmlFor="transport6">
            Otros
          </label>
        </div>
      </div>
    </div>
  );
}
