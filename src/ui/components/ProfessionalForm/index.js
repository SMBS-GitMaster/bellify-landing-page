import React, { useState, Fragment } from "react";
import axios from "axios";
import Step1 from "./step_1";
import Step2 from "./step_2";
import Step3 from "./step_3";
import Step4 from "./step_4";
import Step5 from "./step_5";
import Step6 from "./step_6";
import Step7 from "./step_7";
import Step8 from "./step_8";
import Step9 from "./step_9";
import Step10 from "./step_10";
import Step11 from "./step_11";
import Step12 from "./step_12";
import Step13 from "./step_13";
import Step14 from "./step_14";
import OkMessage from "./greeting";
import StepperProgress from "./progress";
import checkRequiredFields from "../../../services/checkRequiredFields";

export default function ProfessionalForm() {
  const [step, setStep] = useState(1);
  const [showOk, setShowOk] = useState(false);
  const [state, setState] = useState({});
  const [showRequiredError, setShowRequiredError] = useState(false);
  const requiredFields = ["name", "lastname", "phone", "email", "birth_day"];
  const stepCount = 14;

  const handleChange = (name, value) => {
    setState({ ...state, [name]: value });
  };

  const submitForm = () => {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    axios
      .post(
        "https://demo1.beonshop.com/bellifyapi/public/api/applicants",
        { ...state },
        config
      )
      .then(() => {
        setShowOk(true);
        setStep(stepCount + 1);
      })
      .catch((error) => {
        console.log("error", error);
      });
  };

  const back = () => {
    setStep(step - 1);
  };

  const next = () => {
    if (!checkRequiredFields(requiredFields, state)) {
      setShowRequiredError(true);
      return;
    } else {
      setShowRequiredError(false);
    }

    if (step < stepCount) {
      setStep(step + 1);
    }

    //Final option, submit the form.
    if (step === stepCount) {
      submitForm();
    }
  };

  const renderButtons = () => {
    if (showOk) {
      return <Fragment></Fragment>;
    }

    return (
      <div className="StepperForm-buttons">
        {step > 1 && (
          <button onClick={back} className="StepperForm-button left">
            <i className="StepperForm-arrow left"></i>
          </button>
        )}
        <button onClick={next} className="StepperForm-button right">
          <i className="StepperForm-arrow right"></i>
        </button>
      </div>
    );
  };

  return (
    <div className="StepperForm">
      <StepperProgress
        display={!showOk}
        currentStep={step}
        stepCount={stepCount}
      />
      <div className="StepperForm-content">
        <Step1 handleChange={handleChange} display={step === 1} />
        <Step2 handleChange={handleChange} display={step === 2} />
        <Step3 handleChange={handleChange} display={step === 3} />
        <Step4 handleChange={handleChange} display={step === 4} />
        <Step5 handleChange={handleChange} display={step === 5} />
        <Step6 handleChange={handleChange} display={step === 6} />
        <Step7 handleChange={handleChange} display={step === 7} />
        <Step8 handleChange={handleChange} display={step === 8} />
        <Step9 handleChange={handleChange} display={step === 9} />
        <Step10 handleChange={handleChange} display={step === 10} />
        <Step11 handleChange={handleChange} display={step === 11} />
        <Step12 handleChange={handleChange} display={step === 12} />
        <Step13 handleChange={handleChange} display={step === 13} />
        <Step14 handleChange={handleChange} display={step === 14} />
        <OkMessage display={showOk} />
      </div>
      {showRequiredError && (
        <div class="alert alert-danger" role="alert">
          Debe llenar todos los campos requeridos antes de continuar
        </div>
      )}
      {renderButtons()}
    </div>
  );
}
