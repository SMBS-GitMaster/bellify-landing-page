import React from "react";

export default function Step8(props) {
  const handleChange = ({ target: { name, value } }) => {
    props.handleChange(name, value);
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">¿Cómo conociste Bellify?</span>
      </div>
      <div className="col-12">
        <textarea
          className="BenefitsForm-textArea p-4"
          rows="6"
          onChange={handleChange}
          name="how_know_bellify"
          placeholder="Escribe aqui tu respuesta"
        />
      </div>
    </div>
  );
}
