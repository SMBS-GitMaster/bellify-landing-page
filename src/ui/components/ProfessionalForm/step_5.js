import React, { Fragment, useState } from "react";
import MaterialInput from "../MaterialInput";

export default function Step5(props) {
  const [disableInput, setDisableInput] = useState(true);

  const handleChange = ({ target: { name, value, checked } }) => {
    if (checked) {
      props.handleChange(name, value);
    } else {
      props.handleChange(name, null);
    }
  };

  const handleTogglerCheck = ({ target: { checked } }) => {
    setDisableInput(!checked);
  };

  const inputLabel = () => {
    return (
      <Fragment>
        <span className="d-none d-sm-inline">
          En caso de seleccionar Otros,{" "}
        </span>
        <span>Indica el animal por favor</span>
      </Fragment>
    );
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Eres alérgico a algún animal?
        </span>
      </div>
      <div className="BenefitsForm-checkRow">
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="allergic"
            value="Perros"
            id="allergic_dog"
          />
          <label className="pl-2" htmlFor="allergic_dog">
            Perros
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="allergic"
            value="Gatos"
            id="allergic_cat"
          />
          <label className="pl-2" htmlFor="allergic_cat">
            Gatos
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleTogglerCheck}
            name="allergic_other"
            id="allergic_other"
          />
          <label className="pl-2" htmlFor="allergic_other">
            Otros
          </label>
        </div>
      </div>
      <div className="col-12">
        <MaterialInput
          name="allergic"
          onChange={props.handleChange}
          id="allergic"
          label={inputLabel}
          disabled={disableInput}
        />
      </div>
    </div>
  );
}
