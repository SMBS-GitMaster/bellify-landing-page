import React, { useState } from "react";
import InputNumber from "rc-input-number";
import "rc-input-number/assets/index.css";

export default function Step2(props) {
  const [state, setState] = useState({
    hairdressing_level: 0,
    makeup_level: 0,
    nail_level: 0,
    esthetic_level: 0,
  });

  const handleChecks = ({ target: { name, checked } }) => {
    switch (name) {
      case "peluqueria":
        if (!checked) {
          props.handleChange("hairdressing_level", 0);
          setState({ ...state, hairdressing_level: 0 });
        }
        break;
      case "maquillaje":
        if (!checked) {
          props.handleChange("makeup_level", 0);
          setState({ ...state, makeup_level: 0 });
        }
        break;
      case "nails":
        if (!checked) {
          props.handleChange("nail_level", 0);
          setState({ ...state, nail_level: 0 });
        }
        break;
      case "estetica":
        if (!checked) {
          props.handleChange("esthetic_level", 0);
          setState({ ...state, esthetic_level: 0 });
        }
        break;
      default:
    }
    setState({ ...state, [name]: checked });
  };

  const handleHairDressChange = (value) => {
    setState({ ...state, hairdressing_level: value });
    props.handleChange("hairdressing_level", value);
  };

  const handleMakeupChange = (value) => {
    setState({ ...state, makeup_level: value });
    props.handleChange("makeup_level", value);
  };

  const handleNailChange = (value) => {
    setState({ ...state, nail_level: value });
    props.handleChange("nail_level", value);
  };

  const handleEstheticChange = (value) => {
    props.handleChange("esthetic_level", value);
    setState({ ...state, esthetic_level: value });
  };

  const upHandler = <span>▲</span>;
  const downHandler = <span>▼</span>;

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          Selecciona tus areas de trabajo y años de experiencia
        </span>
      </div>
      <div className="col-12 col-xl-3">
        <div className="BenefitsForm-checkbox text-left">
          <input
            onChange={handleChecks}
            type="checkbox"
            name="peluqueria"
            id="peluqueria"
          />
          <label className="pl-2" htmlFor="peluqueria">
            Peluquería
          </label>
          <InputNumber
            style={{ height: 60, display: "block", maxWidth: 150 }}
            defaultValue={0}
            min={0}
            step={1}
            upHandler={upHandler}
            downHandler={downHandler}
            onChange={handleHairDressChange}
            disabled={!state.peluqueria}
            value={state.hairdressing_level}
          />
          {/* <input disabled={!state.peluqueria} className="BenefitsForm-numberInput" type="number" min={0} step={1} name="hairdressing_level" onChange={handleChange} id="hairdressing_level"/> */}
        </div>
      </div>
      <div className="col-12 col-xl-3">
        <div className="BenefitsForm-checkbox text-left">
          <input
            onChange={handleChecks}
            type="checkbox"
            name="maquillaje"
            id="maquillaje"
          />
          <label className="pl-2" htmlFor="maquillaje">
            Maquillaje
          </label>
          <InputNumber
            style={{ height: 60, display: "block", maxWidth: 150 }}
            defaultValue={0}
            min={0}
            step={1}
            upHandler={upHandler}
            downHandler={downHandler}
            onChange={handleMakeupChange}
            disabled={!state.maquillaje}
            value={state.makeup_level}
          />
          {/* <input disabled={!state.maquillaje} className="BenefitsForm-numberInput" type="number" min={0} step={1} name="makeup_level" onChange={handleChange} id="makeup_level"/> */}
        </div>
      </div>
      <div className="col-12 col-xl-3">
        <div className="BenefitsForm-checkbox text-left">
          <input
            onChange={handleChecks}
            type="checkbox"
            name="nails"
            id="nails"
          />
          <label className="pl-2" htmlFor="nails">
            Uñas
          </label>
          <InputNumber
            style={{ height: 60, display: "block", maxWidth: 150 }}
            defaultValue={0}
            min={0}
            step={1}
            upHandler={upHandler}
            downHandler={downHandler}
            onChange={handleNailChange}
            disabled={!state.nails}
            value={state.nail_level}
          />
          {/* <input disabled={!state.nails} className="BenefitsForm-numberInput" type="number" min={0} step={1} name="nail_level" onChange={handleChange} id="nail_level"/> */}
        </div>
      </div>
      <div className="col-12 col-xl-3">
        <div className="BenefitsForm-checkbox text-left">
          <input
            onChange={handleChecks}
            type="checkbox"
            name="estetica"
            id="estetica"
          />
          <label className="pl-2" htmlFor="estetica">
            Estética
          </label>
          <InputNumber
            style={{ height: 60, display: "block", maxWidth: 150 }}
            defaultValue={0}
            min={0}
            step={1}
            upHandler={upHandler}
            downHandler={downHandler}
            onChange={handleEstheticChange}
            disabled={!state.estetica}
            value={state.esthetic_level}
          />
          {/* <input disabled={!state.estetica} className="BenefitsForm-numberInput" type="number" min={0} step={1} name="esthetic_level" onChange={handleChange} id="esthetic_level"/> */}
        </div>
      </div>
    </div>
  );
}
