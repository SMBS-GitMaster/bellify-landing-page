import React, { Fragment } from "react";
import BannerSection from "../Banner";
import HomeService from "./homeServices";
import HomeBenefits from "./homeBenefits";
import HomeProfessionals from "./homeProfessionals";
import Newsletter from "./newsletter";
import Footer from "../Footer";

import mainBanner from "../../../assets/images/banners/main-banner.jpg";
import logo from "../../../assets/images/logo.png";
import mobileLogo from "../../../assets/images/mobile-logo.png";

export default function Home() {
  const goToSalon = () => {
    const section = document.getElementById("HomeBenefits");
    section.scrollIntoView({ behavior: "smooth" });
  };

  const goToProfessional = () => {
    const section = document.getElementById("Professionals");
    section.scrollIntoView({ behavior: "smooth" });
  };

  const goToNewsletter = () => {
    const section = document.getElementById("Newsletter");
    section.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <Fragment>
      <div className="WrapperBanner">
        <div className="BannerSection-mobileLogo">
          <img src={mobileLogo} alt="Bellify" />
        </div>
        <BannerSection fileName={mainBanner}>
          <img src={logo} alt="Bellify" className="BannerSection-logo" />
          <div className="BannerSection-block">
            <h1 className="BannerSection-title">
              Tu asesor de belleza a domicilio
            </h1>
            <p className="BannerSection-text d-none d-lg-block">
              El lanzamiento más esperado en el sector de la belleza
            </p>
            {/* <p className="BannerSection-text d-none d-lg-block">Productos imprescindibles.</p>
                        <p className="BannerSection-text d-none d-lg-block">En cualquier lugar, en cualquier lugar momento.</p> */}
            <p className="BannerSection-text large">Próximamente</p>
          </div>
          <div className="row w-100">
            <div className="col-12 col-md-4 mb-3">
              <button onClick={goToSalon} className="BannerSection-button">
                Quiero unirme como salón
              </button>
            </div>
            <div className="col-12 col-md-4 mb-3">
              <button
                onClick={goToProfessional}
                className="BannerSection-button"
              >
                Quiero unirme como profesional
              </button>
            </div>
            <div className="col-12 col-md-4 mb-3">
              <button
                onClick={goToNewsletter}
                className="BannerSection-button white"
              >
                Quiero estar informado del lanzamiento
              </button>
            </div>
          </div>
          <i className="BannerSection-arrow"></i>
        </BannerSection>
      </div>
      <HomeService />
      <HomeBenefits />
      <HomeProfessionals />
      <Newsletter />
      <Footer />
    </Fragment>
  );
}
