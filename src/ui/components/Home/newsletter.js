import React from "react";
import MaterialInput from "../MaterialInput";

export default function Newsletter() {
  return (
    <div className="Newsletter" id="Newsletter">
      <div className="Newsletter-container">
        <h5 className="Newsletter-title">
          <span>Se el primero en enterarte del día de lanzamiento</span>
        </h5>
        <span className="Newsletter-description">
          Estamos calentando motores para que el lanzamiento tan esperado en el
          sector de la belleza sea un éxito. Para ello tenemos muchas sorpresas
          para ti, regístrate y se la primera en beneficiarte de las promociones
          del lanzamiento
        </span>
        <form className="NewsForm">
          <div className="NewsForm-inputWrap">
            <MaterialInput
              className="NewsForm-input"
              label="Correo electrónico"
            />
            <button className="NewsForm-submit">
              <i className="NewsForm-arrowRight"></i>
            </button>
          </div>
          <div className="Newsletter-terms">
            <input type="checkbox" name="accept_terms" id="accept_terms" />
            <label className="pl-2">
              Confirmo que haber leído y estar conforme con los{" "}
              <a href="/">términos y condiciones generales</a> y la{" "}
              <a href="/">política de privacidad de Bellify.</a>
            </label>
          </div>
        </form>
      </div>
    </div>
  );
}
