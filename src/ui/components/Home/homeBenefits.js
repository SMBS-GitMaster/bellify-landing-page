import React from "react";
import BenefitsForm from "../BenefitsForm";
import eyelash from "../../../assets/images/icons/eyelash.svg";
import scissors from "../../../assets/images/icons/scissors.svg";
import faces from "../../../assets/images/icons/faces.svg";
import arrow_dots from "../../../assets/images/icons/arrow-dots.svg";

export default function HomeBenefits() {
  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className="HomeBenefits" id="HomeBenefits">
      <div className="HomeBenefits-container">
        <h2 className="HomeBenefits-title">
          <span>Los beneficios de participar con tu salón de belleza</span>
        </h2>
      </div>
      <div className="HomeBenefits-container">
        <div className="row">
          <div className="col-md-6 col-xl-3">
            <div className="HomeBenefits-item visibility">
              <img src={eyelash} alt="" />
              <h5>Gana visibilidad y clientes</h5>
              <span>
                Accede a nuevos clientes, gana visibilidad y potencia tus
                servicios a través de la red de colaboradores de Bellify en
                hoteles, eventos y empresas
              </span>
            </div>
          </div>
          <div className="col-md-6 col-xl-3">
            <div className="HomeBenefits-item">
              <img src={scissors} alt="" />
              <h5>Optimiza los recursos del salón</h5>
              <span>
                Incrementa la flexibilidad horaria, amplia el horario comercial
                sin incrementar costes y optimiza tus recursos en función de la
                demanda
              </span>
            </div>
          </div>
          <div className="col-md-6 col-xl-3">
            <div className="HomeBenefits-item">
              <img src={faces} alt="" />
              <h5>Accede a un amplia red de profesionales</h5>
              <span>
                Accede a una amplia red de profesionales autónomos que permitan
                optimizar tus costes de personal
              </span>
            </div>
          </div>
          <div className="col-md-6 col-xl-3">
            <div className="HomeBenefits-item">
              <img src={arrow_dots} alt="" />
              <h5>Incrementa la rentabilidad</h5>
              <span>
                Incrementa ingresos por servicios, obtén beneficios por la venta
                de productos online y benefíciate de la inversión en marketing,
                publicidad y relaciones públicas de Bellify
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="HomeBenefits-container">
        <form className="BenefitsForm" onSubmit={handleSubmit}>
          <div className="BenefitsForm-wrapper">
            <h2 className="BenefitsForm-title">
              <span>Regístrate con tu salón de belleza</span>
            </h2>
            <span className="BenefitsForm-description">
              Completa el siguiente formulario y recibirás más información sobre
              como se podrá beneficiar tu salón de belleza de las múltiples
              ventajas de la comunidad Bellify
            </span>
          </div>
          <div className="BenefitsForm-wrapper">
            <BenefitsForm />
          </div>
        </form>
      </div>
    </div>
  );
}
