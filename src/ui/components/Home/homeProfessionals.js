import React from "react";
import ProfessionalForm from "../ProfessionalForm";
import hand_up from "../../../assets/images/icons/hand-up.svg";
import couch from "../../../assets/images/icons/couch.svg";
import salon from "../../../assets/images/icons/salon-mirrors.svg";
import bag from "../../../assets/images/icons/makeup-bag.svg";

export default function HomeProfessionals() {
  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className="Professionals" id="Professionals">
      <div className="Professionals-container">
        <h2 className="Professionals-title">
          <span>Los beneficios de unirte como profesional</span>
        </h2>
      </div>
      <div className="Professionals-container">
        <div className="row">
          <div className="col-md-6 col-xl-3">
            <div className="Professionals-item">
              <img src={hand_up} alt="" />
              <h5>Accede a una amplia cartera de clientes</h5>
              <span>
                Gestiona tu propia cartera de clientes, gana visibilidad y
                potencia tus servicios a través de la red de colaboradores de
                Bellify en hoteles, eventos y empresas
              </span>
            </div>
          </div>
          <div className="col-md-6 col-xl-3">
            <div className="Professionals-item">
              <img src={couch} alt="" />
              <h5>Se tu propio jefe</h5>
              <span>
                Gestiona tu propio tiempo, accede a un amplio horario comercial,
                selecciona tus propias zonas de trabajo y concilia el trabajo
                con la familia
              </span>
            </div>
          </div>
          <div className="col-md-6 col-xl-3">
            <div className="Professionals-item mirrors">
              <img src={salon} alt="" />
              <h5>Accede a un amplia red de salones de belleza</h5>
              <span>
                Accede a una amplia red de salones asociados a Bellify con quien
                podrás colaborar y potenciar tus ingresos recurrentes
              </span>
            </div>
          </div>
          <div className="col-md-6 col-xl-3">
            <div className="Professionals-item">
              <img src={bag} alt="" />
              <h5>Genera más ingresos</h5>
              <span>
                Accede a una amplia red de salones asociados a Bellify con quien
                podrás colaborar y potenciar tus ingresos recurrentes
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="HomeBenefits-container">
        <form className="BenefitsForm" onSubmit={handleSubmit}>
          <div className="BenefitsForm-wrapper">
            <h2 className="BenefitsForm-title">
              <span>Registrate como profesional</span>
            </h2>
            <span className="BenefitsForm-description">
              Completa el siguiente formulario y recibirás más información sobre
              como podrás beneficiarte de las múltiples ventajas de la comunidad
              Bellify
            </span>
          </div>
          <div className="BenefitsForm-wrapper">
            <ProfessionalForm />
          </div>
        </form>
      </div>
    </div>
  );
}
