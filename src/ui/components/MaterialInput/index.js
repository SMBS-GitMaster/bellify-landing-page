import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";

export default function MaterialInput(props) {
  const [showInvalidDate, setShowInvalidDate] = useState(false);

  const handleChange = ({ target: { name, value } }) => {
    if (props.type === "date" && value !== "") {
      const date = moment(value).isValid();
      setShowInvalidDate(!date);
    } else {
      setShowInvalidDate(false);
    }

    if (props.onChange && typeof props.onChange === "function") {
      props.onChange(name, value, props.isRequired);
    }
  };

  const renderLabel = () => {
    if (typeof props.label === "function") {
      return <Fragment>{props.label()}</Fragment>;
    }

    return <Fragment>{props.label}</Fragment>;
  };

  return (
    <div
      className={`MaterialInput${props.disabled ? " disabled" : ""} ${
        props.className
      }`}
    >
      <input
        placeholder="default"
        onChange={handleChange}
        id={props.id}
        value={props.value}
        name={props.name}
        type={props.type}
        disabled={props.disabled}
        className="MaterialInput-input"
      />
      <label className="MaterialInput-label">{renderLabel()}</label>
      {showInvalidDate && (
        <span className="MaterialInput-invalid">
          Formato de fecha no válido
        </span>
      )}
    </div>
  );
}

MaterialInput.defaultProps = {
  className: "",
  type: "text",
  disabled: false,
  isRequired: false,
};

MaterialInput.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any,
  className: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  isRequired: PropTypes.bool,
};
