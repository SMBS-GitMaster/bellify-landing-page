import React from 'react';
import PropTypes from 'prop-types';

export default function Banner(props){
    return(
        <div className={`BannerSection`} style={{backgroundImage:`url(${props.fileName})`}}>
            <div className="container BannerSection-content">
                {props.children}
            </div>
        </div>
    );
}

Banner.propTypes = {
    fileName: PropTypes.string,
}
