import React, {Fragment} from 'react';
import PropTypes from 'prop-types';

export default function StepperProgress(props) {
    if(!props.display){
        return <Fragment></Fragment>
    }

    return(
        <div className="ProgressBar">
            <span className="ProgressBar-count">{`${props.currentStep}/${props.stepCount}`}</span>
            <div className="ProgressBar-outside">
                <span 
                    className="ProgressBar-inside"
                    style={{ width: `${(props.currentStep * 100) / props.stepCount}%` }}
                ></span>
            </div>
        </div>
    );
}

StepperProgress.propTypes = {
    currentStep: PropTypes.number.isRequired,
    stepCount: PropTypes.number.isRequired,
    display: PropTypes.bool
}

