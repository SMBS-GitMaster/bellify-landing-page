import React from "react";
import MaterialInput from "../MaterialInput";

export default function Step1(props) {
  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12 col-xl-6">
        <MaterialInput
          className="BenefitsForm-input"
          label="Nombre*"
          name="name"
          onChange={props.handleChange}
          isRequired
        />
        <MaterialInput
          className="BenefitsForm-input"
          label="Apellidos*"
          name="lastname"
          onChange={props.handleChange}
          isRequired
        />
        <MaterialInput
          className="BenefitsForm-input"
          label="Número de DNI / NIE"
          name="dni"
          onChange={props.handleChange}
        />
        <MaterialInput
          className="BenefitsForm-input"
          label="Dirección del salón de belleza"
          name="address"
          onChange={props.handleChange}
        />
      </div>
      <div className="col-12 col-xl-6">
        <MaterialInput
          className="BenefitsForm-input"
          label="Código postal del salón de belleza"
          name="zip"
          onChange={props.handleChange}
        />
        <MaterialInput
          className="BenefitsForm-input"
          label="Teléfono de contacto*"
          name="phone"
          onChange={props.handleChange}
          isRequired
        />
        <MaterialInput
          className="BenefitsForm-input"
          label="Correo electrónico*"
          name="email"
          onChange={props.handleChange}
          isRequired
          type="email"
        />
        <MaterialInput
          className="BenefitsForm-input"
          label="Fecha de nacimiento*"
          name="birth_day"
          onChange={props.handleChange}
          isRequired
          type="date"
        />
      </div>
      <div className="col-12 col-xl-6">
        <span className="BenefitsForm-text font-weight-light d-block mt-2 mb-4">
          * Campos obligatorios
        </span>
      </div>
    </div>
  );
}
