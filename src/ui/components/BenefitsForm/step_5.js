import React, { Fragment } from "react";
import MaterialInput from "../MaterialInput";

export default function Step5(props) {
  const handleChange = ({ target: { name, value, checked } }) => {
    if (checked) {
      props.handleChange(name, value);
    } else {
      props.handleChange(name, null);
    }
  };

  const inputLabel = () => {
    return (
      <Fragment>
        <span className="d-none d-sm-inline">En caso de mas de 3, </span>
        <span>Indica el número exacto aquí</span>
      </Fragment>
    );
  };

  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Cuántos trabajadores tiene el salón de belleza (contándote a ti)?
        </span>
      </div>
      <div className="BenefitsForm-checkRow">
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="number_employees"
            value="1"
            id="allergic_dog"
          />
          <label className="pl-2" htmlFor="allergic_dog">
            1
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input
            type="checkbox"
            onChange={handleChange}
            name="number_employees"
            value="2"
            id="allergic_cat"
          />
          <label className="pl-2" htmlFor="allergic_cat">
            2
          </label>
        </div>
        <div className="BenefitsForm-checkbox">
          <input type="checkbox" name="number_employees" id="allergic_other" />
          <label className="pl-2" htmlFor="allergic_other">
            Más de 3
          </label>
        </div>
      </div>
      <div className="col-12">
        <MaterialInput
          name="number_employees"
          onChange={props.handleChange}
          id="allergic"
          label={inputLabel}
        />
      </div>
    </div>
  );
}
