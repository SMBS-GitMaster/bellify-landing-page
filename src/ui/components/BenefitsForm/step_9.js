import React, {Fragment} from 'react';

export default function Step8(props){    
    if(!props.display) {
        return <Fragment></Fragment>;
    }

    const handleChange = ({target: {name, value}}) => {
        props.handleChange(name, value);
    }

    return(
        <div className="row">
            <div className="col-12">
                <span className="BenefitsForm-subtitle">
                    ¿Qué es lo que te motiva a nivel profesional?
                </span>
            </div>            
            <div className="col-12">
                <textarea className="BenefitsForm-textArea p-4" onChange={handleChange} name="professional_motivation" rows="6" placeholder="Escribe aqui tu respuesta" />
            </div>
        </div>
    );
}