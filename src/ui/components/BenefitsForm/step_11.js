import React, {Fragment} from 'react';

export default function Step8(props){    
    if(!props.display) {
        return <Fragment></Fragment>;
    }

    const handleChange = ({target: {name, value}}) => {
        props.handleChange(name, value);
    }

    return(
        <div className="row">
            <div className="col-12">
                <span className="BenefitsForm-subtitle">
                    ¿Por qué crees que eres un buen profesional para Bellify?
                </span>
            </div>            
            <div className="col-12">
                <textarea className="BenefitsForm-textArea p-4" rows="6" onChange={handleChange} name="reasons" placeholder="Escribe aqui tu respuesta" />
            </div>
        </div>
    );
}