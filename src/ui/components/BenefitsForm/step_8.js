import React, {Fragment} from 'react';
import MaterialInput from '../MaterialInput';

export default function Step8(props){    
    if(!props.display) {
        return <Fragment></Fragment>;
    }

    const handleChange = ({target: {name, value, checked}}) => {
        if(checked) {
            props.handleChange(name, value);
        } else {
            props.handleChange(name, null);            
        }
    }

    return(
        <div className="row">
            <div className="col-12">
                <span className="BenefitsForm-subtitle">
                    ¿Eres puntual?
                </span>
            </div>
            <div className="BenefitsForm-checkRow">
                <div className="BenefitsForm-checkbox">
                    <input type="checkbox" name="is_punctual" onChange={handleChange} value="Si" id="punctual" />
                    <label className="pl-2" htmlFor="punctual">Si</label>
                </div>            
                <div className="BenefitsForm-checkbox">
                    <input type="checkbox" name="is_punctual" onChange={handleChange} value="No" id="punctual2" />
                    <label className="pl-2" htmlFor="punctual2">No</label>
                </div>
            </div>
            <div className="col-12">
                <MaterialInput name="is_punctual" onChange={props.handleChange} label="En caso negativo, indique el motivo de ello" />
            </div>
        </div>
    );
}