import React, {Fragment} from 'react';

export default function Step7(props){    
    if(!props.display) {
        return <Fragment></Fragment>;
    }

    const handleChange = ({target: {name, checked}}) => {
        props.handleChange(name, checked);
    }

    return(
        <div className="row">
            <div className="col-12">
                <span className="BenefitsForm-subtitle">
                    ¿Tienes algún inconveniente para desplazarte a prestar servicios a domicilio del cliente? 
                </span>
            </div>
            <div className="BenefitsForm-checkRow">
                <div className="BenefitsForm-checkbox">
                    <input type="checkbox" name="have_inconvenient" onChange={handleChange} value={true} id="home_service" />
                    <label className="pl-2" htmlFor="home_service">Si</label>
                </div>            
                <div className="BenefitsForm-checkbox">
                    <input type="checkbox" name="have_inconvenient" onChange={handleChange} value={false} id="home_service2" />
                    <label className="pl-2" htmlFor="home_service2">No</label>
                </div>
            </div>                         
        </div>
    );
}