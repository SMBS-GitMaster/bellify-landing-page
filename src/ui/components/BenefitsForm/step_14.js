import React from "react";

export default function Step14(props) {
  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="Greeting-title">¡Muchas gracias!</span>
      </div>
      <div className="col-12">
        <p className="Greeting-text">
          En breve nos pondremos en contacto contigo.
        </p>
      </div>
    </div>
  );
}
