import React from "react";
import MaterialInput from "../MaterialInput";

export default function Step4(props) {
  return (
    <div className={`row${props.display ? "" : " d-none"}`}>
      <div className="col-12">
        <span className="BenefitsForm-subtitle">
          ¿Cuál es el nombre comercial de tu salón?
        </span>
      </div>
      <div className="col-12">
        <MaterialInput
          id="salon_name"
          onChange={props.handleChange}
          name="salon_name"
          label="Escribe aquí tu respuesta"
        />
      </div>
    </div>
  );
}
